
function round(num){
	return Math.round(num * 100) / 100;
}


function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function isValid(loanamount, annualtax, annualinsurance){
	var ret = true;

	if(! isNumber(loanamount)){
		ret = false;
		document.getElementById("loan-amount-group").classList.add("error")
		document.getElementById("loan-amount-error").innerHTML  = "Must be a number";
		document.getElementById("loan-amount-error-desktop").innerHTML  = "Loan Amount must be a number";
	}

	if(! isNumber(annualtax)){
		ret = false;
		document.getElementById("annual-tax-group").classList.add("error")
		document.getElementById("annual-tax-error").innerHTML  = "Must be a number";
		document.getElementById("annual-tax-error-desktop").innerHTML  = "Annual Tax must be a number";
	}

	if(! isNumber(annualinsurance)){
		ret = false;
		document.getElementById("annual-insurance-group").classList.add("error")
		document.getElementById("annual-insurance-error").innerHTML  = "Must be a number";
		document.getElementById("annual-insurance-error-desktop").innerHTML  = "Annual Insurance must be a number";
	}


	if(loanamount==""){
		ret = false;
		document.getElementById("loan-amount-group").classList.add("error")
		document.getElementById("loan-amount-error").innerHTML  = "Mandatory field";
		document.getElementById("loan-amount-error-desktop").innerHTML  = "Loan Amount is mandatory";
	}

	if(annualtax==""){
		ret = false;
		document.getElementById("annual-tax-group").classList.add("error")
		document.getElementById("annual-tax-error").innerHTML  = "Mandatory field";
		document.getElementById("annual-tax-error-desktop").innerHTML  = "Annual Tax is mandatory";
	}

	if(annualinsurance==""){
		ret = false;
		document.getElementById("annual-insurance-group").classList.add("error")
		document.getElementById("annual-insurance-error").innerHTML  = "Mandatory field";
		document.getElementById("annual-insurance-error-desktop").innerHTML  = "Annual Insurance is mandatory";
	}




	return ret;
}



function calculate(){
	yearsofmortgage = document.getElementById("years-of-mortgage").value;
	rateofinterest = document.getElementById("rate-of-interest").value;
	loanamount = document.getElementById("loan-amount").value;
	annualtax = document.getElementById("annual-tax").value;
	annualinsurance = document.getElementById("annual-insurance").value;

	document.getElementById("loan-amount-group").classList.remove("error")
	document.getElementById("annual-tax-group").classList.remove("error")
	document.getElementById("annual-insurance-group").classList.remove("error")

	if(! isValid(loanamount, annualtax, annualinsurance)){
		return;
	}


	document.getElementById("Rectangle-results").classList.remove("empty")

	// document.getElementById("Rectangle-results").classList.add("empty")

	principleinterestvalue = ((rateofinterest / 100) / 12) * loanamount / (1-Math.pow((1 + ((rateofinterest / 100)/12)), -yearsofmortgage*12));
	taxvalue = annualtax / 12;
	insurancevalue = annualinsurance / 12

	totalvalue = principleinterestvalue + taxvalue + insurancevalue

	document.getElementById("principle-interest-value").innerHTML = "$ " + round(principleinterestvalue);
	document.getElementById("tax-value").innerHTML  = "$ " + round(taxvalue);
	document.getElementById("insurance-value").innerHTML  = "$ " + round(insurancevalue);
	document.getElementById("total-value").innerHTML  = "$ " + round(totalvalue);
	document.getElementById("btn-calculate").innerHTML  = "RECALCULATE";
	
}
